# Simulation

1. [Introduction](#Introduction)

2. [Simulation](#Simulation)


## Introduction <a name="Introduction"></a>

Le dossier **Simulation** contient les fichiers _simulation_ qui contiennent les fonctions permettant de lancer sur la simulation.

## Simulation <a name="Simulation"></a>

| Type | Fonction                 | Description                                         |
|------|--------------------------|-----------------------------------------------------|
| List | getAllCharges            | Ajoute toutes les charges à une liste.              |
| VOID | saveListBeforeSimulation | Sauvegarde la liste de charges avant la simulation. |
| INT  | simulation               | Lance la simulation.                                |
