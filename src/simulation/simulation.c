/* File simulationEvent.c */
/* Date: 2020-06-11 */
/* Description: simulation event */

#include "simulation.h"

/* Save list of charges before simulation for stop action */
VOID saveListBeforeSimulation(App *ESSApp, List* chgsSaved) {
    
    AppCharge *appChg;
    AppCharge *appChgSave;
    Charge *chg;
    Charge *chgSave;
    for (INT idx = 0; idx < ESSApp->charges->length; idx++) {
        appChg = ESSApp->charges->GetDataByIndex(ESSApp->charges, idx);
        chg = appChg->chg;
        chgSave = newCharge(newPoint(chg->coord->x, chg->coord->y, chg->coord->z), chg->chgq, chg->fixe);
        appChgSave = newAppCharge(chgSave, appChg->idx, appChg->posX, appChg->posY);
        chgsSaved->Append(chgsSaved, appChgSave);
    }

}

/* Simulation function */   
INT simulation(void * user_data) {

    App *ESSApp = (App*)user_data;
    DOUBLE time = ESSApp->options->simTimeStart;
    DOUBLE size = ESSApp->options->scaleSize;
    AppCharge* appChg;
    Charge *chg;
    List *chgs; 
    Vector* force;
    Vector* acc;
    Vector *spd;
    
    if (ESSApp->simStatus == RUNING) {
        for (INT idx = 0; idx < ESSApp->charges->length; idx++) {
            appChg = ESSApp->charges->GetDataByIndex(ESSApp->charges, idx);
            chg = appChg->chg;
            if (!chg->fixe) {
                chgs = getAllCharges(ESSApp, idx);
                force = chg->coulombLawMultiple(chgs, chg, size);
                acc = acceleration(force, chg);

                appChg->acc = acc;
                if (appChg->initSpeed == NULL)
                    appChg->initSpeed = newVector(0, 0, 0);
                spd = speed(appChg->acc, appChg->initSpeed, time);

                coordinates(appChg->acc, spd, chg->coord, time);

                change_label_info(ESSApp, appChg->idx, appChg->posX, appChg->posY, idx, chg->coord->x, chg->coord->y);
            }
        }
        gtk_widget_queue_draw(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 1));
        ESSApp->options->simTimeStart += ESSApp->options->simTimeIncrement;
    }
    
    return TRUE;

}
