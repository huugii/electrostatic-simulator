/* File eventsBtn.c */
/* Date: 2020-06-17 */
/* Description: Buttons Events */

#include "eventsTools.h"

/*
	Function: on_event_tools_draw
	----------------
	Description: Draw the window ESSApp on the screen
	Return: gboolean
 */
gboolean on_event_tools_draw(GtkWidget *widget, cairo_t *cr, App *ESSApp) {

	GObject *win;
	INT width;
	INT height;

	if (ESSApp->isSettingChange) {
		win = G_OBJECT(ESSApp->widgets->first->data);
		gtk_widget_hide(GTK_WIDGET(win));
		gtk_window_resize(GTK_WINDOW(win), ESSApp->options->winWidth, ESSApp->options->winHeight);
		gtk_window_set_position(GTK_WINDOW(win), GTK_WIN_POS_CENTER);
		gtk_widget_show_all(GTK_WIDGET(win));
		ESSApp->isSettingChange = FALSE;
	}

	width = gtk_widget_get_allocated_width(widget);
	height = gtk_widget_get_allocated_height(widget);
	
	cairo_set_source_rgb(cr, ESSApp->options->colorBg.r, ESSApp->options->colorBg.g, ESSApp->options->colorBg.b);
	cairo_paint(cr);

	if (ESSApp->options->areaWidth != width || ESSApp->options->areaHeight != height) {
		ESSApp->options->posAxeX = width / 2;
		ESSApp->options->posAxeY = height / 2;
	}

	ESSApp->options->areaWidth = width;
	ESSApp->options->areaHeight = height;

	draw_axes(cr, ESSApp);
	draw_lines(cr, ESSApp);
	draw_force_charge(cr, ESSApp);
	draw_charges(cr, ESSApp);

	set_label_characterisics_charges(ESSApp);
	set_label_characterisics_forces(ESSApp);

	displayLogs(ESSApp);
	
	return GDK_EVENT_PROPAGATE;
}

/*
	Function: on_event_tools_move_window
	----------------
	Description: Move the window with mouse right-click
	Return: gboolean
 */
gboolean on_event_tools_move_window(GtkWidget *widget, GdkEventButton *event, App *ESSApp){

	if (event->button == GDK_BUTTON_SECONDARY) {
		if (event->type == GDK_BUTTON_PRESS) {
			gtk_window_begin_move_drag(GTK_WINDOW(gtk_widget_get_toplevel(widget)), event->button, event->x_root, event->y_root, event->time);
		}
	}

	return GDK_EVENT_PROPAGATE;

}

/*
	Function: on_event_tools_cursor_position
	----------------
	Description: Show the cursor cordonate by replacing the label text at bottom left
	Return: gboolean
 */
gboolean on_event_tools_cursor_position(GtkWidget *widget, GdkEvent *event, App *ESSApp) {

	if (event->type == GDK_MOTION_NOTIFY) {
		GdkEventMotion *e = (GdkEventMotion *)event;

		DOUBLE cursorX = (-ESSApp->options->posAxeX + e->x) / ESSApp->options->zoom;
		DOUBLE cursorY = (ESSApp->options->posAxeY - e->y) / ESSApp->options->zoom;

		char arr[10];
		sprintf(arr, "%.lf", cursorX);
		gtk_label_set_text((GtkLabel *)ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 2), arr);
		sprintf(arr, "%.lf", cursorY);
		gtk_label_set_text((GtkLabel *)ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 3), arr);

	}

	return GDK_EVENT_PROPAGATE;

}

/*
	Function: on_event_tools_cursor_measurement
	----------------
	Description: Show the measurements by replacing the label text at the right
	Return: gboolean
 */
gboolean on_event_tools_cursor_measurement(GtkWidget *widget, GdkEvent *event, App *ESSApp){ 

	if (event->type == GDK_MOTION_NOTIFY && ESSApp->mode == MEASUREMENT && ESSApp->charges->length > 0) {
		GdkEventMotion *e = (GdkEventMotion *)event;

		DOUBLE cursorX = (-ESSApp->options->posAxeX + e->x) / ESSApp->options->zoom;
		DOUBLE cursorY = (ESSApp->options->posAxeY - e->y) / ESSApp->options->zoom;

		Point *pnt = newPoint(cursorX, cursorY, 0);
		
		List *charges = newList();
		AppCharge *temp;
		for (INT idx = 0; idx < ESSApp->charges->length; idx++) {
			temp = ESSApp->charges->GetDataByIndex(ESSApp->charges, idx);
			charges->Append(charges, temp->chg);
		}		

		DOUBLE potential = temp->chg->electricPotentialMultiple(charges, pnt, 1E-9 * ESSApp->options->zoom);
		DOUBLE intensity = temp->chg->electricFieldIntensityMultiple(charges, pnt, 1E-9 * ESSApp->options->zoom);
		set_label_characterisics_electrics(ESSApp, potential, intensity);

		DELETE(pnt);
		DELETE(charges);
	}

    return GDK_EVENT_PROPAGATE;

}
