# Charge

1. [Introduction](#Introduction)

2. [Charge](#Charge)


## Introduction <a name="Introduction"></a>

Le dossier **Charge** contient les fichiers _charge_ qui contiennent les fonctions permettant de travailler sur les charges.

## Charge <a name="Charge"></a>

| Type      | Fonction               | Description                                                        |
|-----------|------------------------|--------------------------------------------------------------------|
| VOID      | change_label_size      | Modifie la taille du label de la liste.                            |
| VOID      | change_label_info      | Change le label de la charge.                                      |
| VOID      | add_label_charge_info  | Ajoute un label pour une charge.                                   |
| VOID      | add_charge_on_the_grid | Ajoute une charge sur la grille avec les coordonnées de la grille. |
| AppCharge | searchCharge           | Recherche une charge à proximité aux coordonnées du curseur.       |