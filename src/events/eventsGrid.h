/* File eventsGrid.h */
/* Date: 2020-06-17 */
/* Description: Events Grid objects */

#include "../app/app.h"
#include "../charge/charge.h"

gboolean    on_event_grid_zoom_wheel                (GtkWidget *widget, GdkEvent *event, App *ESSApp);              /* Event [GRID_ZOOM_WHEEL] - when mouse scroll */
gboolean    on_event_grid_move                      (GtkWidget *widget, GdkEvent *event, App *ESSApp);              /* Event [GRID_MOVE] - move grid */
gboolean    on_event_grid_click                     (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [GRID_CLICK] */
gboolean    on_event_grid_move_charge               (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [GRID_MOVE_CHARGE] - when move charge */
gboolean    on_event_grid_charge_add_fixe           (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [GRID_CHARGE_ADD_FIXE] - add fixe charge */
gboolean    on_event_grid_charge_add_mobile         (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [GRID_CHARGE_ADD_MOBILE] - add mobile charge */
gboolean    on_event_grid_charge_delete             (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [GRID_CHARGE_DELETE] - delete a charge */
