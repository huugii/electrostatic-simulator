/* File appLogger.c */
/* Date: 2020-06-19 */
/* Description: App logger functions */

#include "appLogger.h"

/*
    Function: logger_add_log
    ----------------
    Description: Add STRING in a list and display it in app if allowed
    Return: VOID
 */
VOID loggerAddLog(AppLogger *logger, enum ESS_LOGGER_TYPE type, STRING logText) {
    
    /* If log is enable */
    if (!logger->active) {
        return;
    }

    /* Check is the log is allow to be added */
    if (logger->type == DEBUG || logger->type == type) {
    
        time_t now  = time(NULL);
        STRING typeStr;

        switch (type) {
            case ERROR:
                typeStr = "ERROR";
                break;
            case WARNING:
                typeStr = "WARNING";
                break;
            case INFO:
                typeStr = "INFO";
                break;
            case DEBUG:
                typeStr = "DEBUG";
                break;
        }

        STRING text = malloc(_ESS_LOG_TEXT_SIZE_);
        sprintf(text, "%ld - [%s] - %s", now, typeStr, logText);
        logger->logs->Append(logger->logs, text);
    }
}

/*
    Function: newLogger
    ----------------
    Description: Create a new logger
    Return: AppLogger*
 */
AppLogger *newLogger() {
    AppLogger *logger = NEW(AppLogger);

    logger->active  = _ESS_LOG_ACTIVE_;
    logger->type    = _ESS_LOG_TYPE_;
    logger->logs    = newList();

    return logger;
}
