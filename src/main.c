/* File main.c */
/* Date: 2020-06-06 */
/* Description: main ESS */

#include <stdio.h>
#include <stdlib.h>

#include "main.h"

#define _ESS_CSS_FILE_     "ui/themes/ess_theme_basic.css"

/*
	Function: ESSAppInitWidgets
	----------------
	Description: nit widgets of the app
	Return: VOID
 */
VOID ESSAppInitWidgets(App *ESSApp) {

	/* ========== Area Widgets ========== */
	GtkWidget *area;
	area = GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_content_draw_drawarea"));
	gtk_widget_set_size_request(area, ESSApp->options->areaWidth, ESSApp->options->areaHeight);
	gtk_widget_add_events(area, GDK_BUTTON_PRESS_MASK | GDK_POINTER_MOTION_MASK | GDK_SMOOTH_SCROLL_MASK);
	ESSApp->addWidgets(ESSApp, area);              		// 1
	
	g_signal_connect(G_OBJECT(area), "draw", G_CALLBACK(ESSEventDraw), ESSApp);

	g_signal_connect(G_OBJECT(area), "motion-notify-event",G_CALLBACK(ESSEvent), newEvent(ESSApp, GRID_MOVE));
	g_signal_connect(G_OBJECT(area), "motion-notify-event",G_CALLBACK(ESSEvent), newEvent(ESSApp, TOOLS_CURSOR_POSITION));
	g_signal_connect(G_OBJECT(area), "motion-notify-event",G_CALLBACK(ESSEvent), newEvent(ESSApp, TOOLS_CURSOR_MEASUREMENT));
	g_signal_connect(G_OBJECT(area), "motion-notify-event",G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, GRID_MOVE_CHARGE));
	g_signal_connect(G_OBJECT(area), "button-press-event",G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, GRID_CHARGE_DELETE));
	g_signal_connect(G_OBJECT(area), "button-press-event",G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, GRID_CHARGE_ADD_FIXE));
	g_signal_connect(G_OBJECT(area), "button-press-event",G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, GRID_CHARGE_ADD_MOBILE));
	g_signal_connect(G_OBJECT(area), "button-press-event",G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, GRID_CLICK));
	g_signal_connect(G_OBJECT(area), "scroll-event", G_CALLBACK(ESSEvent), newEvent(ESSApp, GRID_ZOOM_WHEEL));

	/* ========== Labels Widgets ========== */
	GtkWidget *labelVersion, *labelPosX, *labelPosY, *labelSizeList,
		*labelChrgPosX, *labelChrgPosY, *labelChrgMass, *labelChrgCharge, 
		*labelVeloPosX, *labelVeloPosY, *labelAccPosX, *labelAccPosY, 
		*labelForceCoulombX, *labelForceCoulombY, *labelElectricPotent, *labelElectricIntens;

	labelPosX 			= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_lbl_info_pos_x"));
	labelPosY 			= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_lbl_info_pos_y"));
	labelSizeList 		= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_lbl_size_list"));

	labelChrgPosX 		= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_lbl_chrg_pos_x"));
	labelChrgPosY 		= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_lbl_chrg_pos_y"));
	labelChrgMass 		= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_lbl_chrg_mass"));
	labelChrgCharge 	= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_lbl_chrg_charge"));
	labelVeloPosX 		= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_lbl_chrg_velocity_x"));
	labelVeloPosY 		= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_lbl_chrg_velocity_y"));
	labelAccPosX 		= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_lbl_chrg_acceleration_x"));
	labelAccPosY 		= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_lbl_chrg_acceleration_y"));
	
	labelForceCoulombX	= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_lbl_force_coulomb_x"));
	labelForceCoulombY	= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_lbl_force_coulomb_y"));

	labelElectricPotent = GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_lbl_electric_potential"));
	labelElectricIntens = GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_lbl_electric_intensity"));
	

	ESSApp->addWidgets(ESSApp, labelPosX);    			// 2
	ESSApp->addWidgets(ESSApp, labelPosY);    			// 3
	ESSApp->addWidgets(ESSApp, labelChrgPosX);    		// 4
	ESSApp->addWidgets(ESSApp, labelChrgPosY);    		// 5
	ESSApp->addWidgets(ESSApp, labelChrgMass);    		// 6
	ESSApp->addWidgets(ESSApp, labelChrgCharge);    	// 7
	ESSApp->addWidgets(ESSApp, labelVeloPosX);   		// 8
	ESSApp->addWidgets(ESSApp, labelVeloPosY);   		// 9
	ESSApp->addWidgets(ESSApp, labelAccPosX);    		// 10
	ESSApp->addWidgets(ESSApp, labelAccPosY);    		// 11
	ESSApp->addWidgets(ESSApp, labelForceCoulombX);    	// 12
	ESSApp->addWidgets(ESSApp, labelForceCoulombY);    	// 13
	ESSApp->addWidgets(ESSApp, labelElectricPotent);    // 14
	ESSApp->addWidgets(ESSApp, labelElectricIntens); 	// 15
	ESSApp->addWidgets(ESSApp, labelElectricIntens); 	// 16
	ESSApp->addWidgets(ESSApp, labelSizeList);			// 17

	/* ========== Contents Widgets ========== */
	GtkWidget *gridChargesInfo, *logArea;
	gridChargesInfo = GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_scroll_charges_list_view_content"));
	logArea = GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_footer_log_area_view_content"));
	ESSApp->addWidgets(ESSApp, gridChargesInfo);    	// 18
	ESSApp->addWidgets(ESSApp, logArea);    			// 19

	/* ========== Button Widgets ========== */
	GtkWidget *buttonView, *buttonFixe, *buttonMobile, *buttonRand,
		*buttonCenter, *buttonSelect, *buttonMeasure, *buttonEdit, 
		*buttonDelete, *buttonClear, *buttonPlay, *buttonPause, 
		*buttonStop, *buttonReplay, *buttonSettings, *buttonHelp, 
		*buttonSpeedUp, *buttonSpeedDown;

	buttonView 		= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_content_tools"));
	buttonFixe 		= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_btn_add_chrg_fixe"));
	buttonMobile 	= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_btn_add_chrg_mobile"));
	buttonRand 		= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_btn_add_chrg_rand"));
	buttonCenter 	= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_btn_control_center"));
	buttonSelect 	= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_btn_control_select"));
	buttonMeasure	= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_btn_control_measurment"));
	buttonEdit 		= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_btn_control_edit"));
	buttonDelete 	= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_btn_control_delete"));
	buttonClear 	= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_btn_control_clear"));

	buttonPlay 		= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_btn_simulation_play"));
	buttonPause 	= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_btn_simulation_pause"));
	buttonStop 		= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_btn_simulation_stop"));
	buttonReplay 	= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_btn_simulation_replay"));
	buttonSpeedUp 	= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_btn_simulation_speed_up"));
	buttonSpeedDown	= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_btn_simulation_speed_down"));

	buttonSettings 	= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_btn_global_settings"));
	buttonHelp 		= GTK_WIDGET(gtk_builder_get_object(ESSApp->Builder, "gui_btn_global_help"));

	ESSApp->addWidgets(ESSApp, buttonPlay);    		// 20
	ESSApp->addWidgets(ESSApp, buttonPause);    	// 21
	ESSApp->addWidgets(ESSApp, buttonStop);    		// 22
	ESSApp->addWidgets(ESSApp, buttonReplay);   	// 23
	ESSApp->addWidgets(ESSApp, buttonSpeedUp);  	// 22
	ESSApp->addWidgets(ESSApp, buttonSpeedDown);   	// 23

	g_signal_connect(G_OBJECT(buttonView), 		"focus", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_CURSOR_DEFAULT));
	g_signal_connect(G_OBJECT(buttonFixe), 		"button-press-event", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_CHARGE_ADD_FIXE));
	g_signal_connect(G_OBJECT(buttonMobile), 	"button-press-event", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_CHARGE_ADD_MOBILE));
	g_signal_connect(G_OBJECT(buttonRand), 		"button-press-event", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_CHARGE_ADD_RANDOM));
	g_signal_connect(G_OBJECT(buttonClear), 	"button-press-event", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_GRID_CLEAR));
	g_signal_connect(G_OBJECT(buttonCenter), 	"button-press-event", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_GRID_CENTER));
	g_signal_connect(G_OBJECT(buttonSelect), 	"button-press-event", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_CURSOR_DEFAULT));
	g_signal_connect(G_OBJECT(buttonMeasure),  	"button-press-event", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_CURSOR_MEASUREMENT));
	g_signal_connect(G_OBJECT(buttonEdit), 		"button-press-event", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_CHARGE_EDIT));
	g_signal_connect(G_OBJECT(buttonDelete), 	"button-press-event", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_CHARGE_DELETE));

	g_signal_connect(G_OBJECT(buttonPlay), 		"button-press-event", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_SIMULATION_PLAY));
	g_signal_connect(G_OBJECT(buttonPause), 	"button-press-event", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_SIMULATION_PAUSE));
	g_signal_connect(G_OBJECT(buttonStop), 		"button-press-event", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_SIMULATION_STOP));
	g_signal_connect(G_OBJECT(buttonReplay), 	"button-press-event", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_SIMULATION_REPLAY));
	g_signal_connect(G_OBJECT(buttonSpeedUp), 	"button-press-event", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_SIMULATION_SPEEDUP));
	g_signal_connect(G_OBJECT(buttonSpeedDown), "button-press-event", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_SIMULATION_SPEEDDOWN));

	g_signal_connect(G_OBJECT(buttonSettings), 	"button-press-event", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_SELECT_SETTINGS));
	g_signal_connect(G_OBJECT(buttonHelp), 	"button-press-event", G_CALLBACK(ESSEventBtn), newEventBtn(ESSApp, MENU_SELECT_HELP));

}

/* ESS Main Function */
VOID ESSAppMain(GtkApplication* GtkApp, gpointer data) {

	App *ESSApp = newApp(GtkApp, gtk_builder_new());

	GtkWidget *Window;
	GtkCssProvider *Provider;
	GError *error = NULL;
	
	/* Builder */
	INT status;
	status = gtk_builder_add_from_file (ESSApp->Builder, _ESS_GLADE_APP_, &error);
	if (status == 0) {
		g_printerr ("Error loading file: %s\n", error->message);
		g_clear_error (&error);
	}

	/* Init Window */
	Window = (GtkWidget*)(gtk_builder_get_object (ESSApp->Builder, "gui_window"));
	gtk_window_set_default_size(GTK_WINDOW(Window), ESSApp->options->winWidth, ESSApp->options->winHeight);
	gtk_label_set_text(GTK_LABEL(gtk_builder_get_object(ESSApp->Builder, "gui_lbl_info_pos_y")), _ESS_APP_VERSION_);
	g_signal_connect(G_OBJECT(Window), "destroy", G_CALLBACK(gtk_main_quit), NULL);

	gtk_application_add_window(GtkApp, GTK_WINDOW(Window));
	ESSApp->addWidgets(ESSApp, Window);

	/* Init CSS */
	Provider = gtk_css_provider_new();
	gtk_css_provider_load_from_file(Provider, g_file_new_for_path(_ESS_CSS_FILE_), &error);

	/* Init Widgets */
	ESSAppInitWidgets(ESSApp);

    /* Intializes random number generator */
    srand((unsigned) time(0));
	
	/* Show all widgets on window */
	gtk_widget_show(Window);
	gtk_main();
}
