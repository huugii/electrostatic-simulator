# Popups

1. [Introduction](#Introduction)

2. [Popup Charges](#Charges)

3. [Popup Options](#Options)


## Introduction <a name="Introduction"></a>

Le dossier **popups** contient les fichiers _popupCharges_ et _popupOptions_ qui contiennent les fonctions permettant d'afficher et de travailler sur les popUp.

## Popup charges <a name="Charges"></a>

Les fichiers _popupCharges_ permettent d'ouvrir une popup et de séléctionner si nous souhaitons une charge positive ou négative.

| Type     | Fonction                 | Description                                                              |
|----------|--------------------------|--------------------------------------------------------------------------|
| VOID     | on_destroy               | Ferme la PopUp.                                                          |
| VOID     | on_choice_click          | Applique des caractéristiques à la charge.                               |
| gboolean | on_choice_click_negative | Retourne un événement GTK si le choix est de placer une charge négative. |
| gboolean | on_choice_click_positive | Retourne un événement GTK si le choix est de placer une charge positive. |
| VOID     | on_popup_charges_click   | Ouvre la popUp lors du click sur le bouton d'ajout de charge.            |


## Popup options <a name="Options"></a>

Les fichiers _popupOptions_ permettent d'ouvrir une popup et d'utilliser la popup des options.

| Type     | Fonction                  | Description                                     |
|----------|---------------------------|-------------------------------------------------|
| gboolean | on_choice_click_cancel    | Ferme la PopUp.                                 |
| gboolean | on_choice_click_save      | Sauvegarde la popUp.                            |
| gboolean | on_choice_click_save_exit | Sauvegarde la popup et quitte cette même popup. |
| VOID     | on_popup_options_clicked  | Ouvre les options de la popup.                  |