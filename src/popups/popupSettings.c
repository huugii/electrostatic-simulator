/* File appOptions.c */
/* Date: 2020-06-14 */
/* Description: ESSApp options */

#include "popupSettings.h"

/*
    Function: on_choice_click_cancel
    ----------------
    Description: Cancel the settings popup (exit)
    Return: gboolean
 */
gboolean on_choice_click_cancel(GtkWidget *widget, GdkEvent *event, Popup *ESSPopup) {
    gtk_window_close(GTK_WINDOW(gtk_widget_get_toplevel(widget)));
    return GDK_EVENT_PROPAGATE;
}

/*
    Function: on_choice_click_save_exit
    ----------------
    Description: Save and Exit the popup
    Return: gboolean
 */
gboolean on_choice_click_save_exit(GtkWidget *widget, GdkEvent *event, Popup *ESSPopup) {
    loggerAddLog(ESSPopup->parentApp->logger, INFO, "Saving settings");
  	savePopupSettings(ESSPopup);
  	gtk_window_close(GTK_WINDOW(gtk_widget_get_toplevel(widget)));
    return GDK_EVENT_PROPAGATE;
}

/*
	Function: on_popup_settings_clicked
	----------------
	Description: Open 
	Return: gboolean
 */
VOID on_popup_settings_clicked(GtkWidget *widget, App *ESSApp) {

    GtkBuilder *builder;
    GtkWidget *popup_window_options;
    GtkWidget *buttonClose, *buttonSaveExit;

    GtkEntry *simTimeStop, *simTimeIncrement;
    CHAR strSimTimeStop[15], strSimTimeIncrement[15];
    
    GtkSpinButton   *winSizeWidth, *winSizeHeight,
                    *spinButtonBR, *spinButtonBG, *spinButtonBB,  /* Colors of Background R, G, B */
                    *spinButtonAR, *spinButtonAG, *spinButtonAB,  /* Colors of Axes R, G, B */
                    *spinButtonLR, *spinButtonLG, *spinButtonLB;  /* Colors of Lines R, G, B */

    /* ========== Init GTK ========== */
    INT status;
    builder = gtk_builder_new();
    status = gtk_builder_add_from_file(builder, _ESS_GLADE_POPUP_SETTINGS_, NULL);
    if (status == 0) {
      	printf("ERROR : %s not found !", _ESS_GLADE_POPUP_SETTINGS_);
        loggerAddLog(ESSApp->logger, DEBUG, "Exiting settings popup");
		gtk_main_quit();
    }

    popup_window_options = GTK_WIDGET(gtk_builder_get_object(builder, "popup_settings"));
    gtk_window_set_transient_for(GTK_WINDOW(popup_window_options), GTK_WINDOW(ESSApp->widgets->first->data));

    buttonClose = GTK_WIDGET(gtk_builder_get_object(builder, "popup_btn_close"));
    buttonSaveExit = GTK_WIDGET(gtk_builder_get_object(builder, "popup_btn_save_exit"));
    
    /* ========== Init General Values ========== */
	loggerAddLog(ESSApp->logger, DEBUG, "Initing General Values");
    winSizeWidth = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "popup_entry_general_sizex"));
    gtk_spin_button_set_value(winSizeWidth, (DOUBLE)ESSApp->options->winWidth);

    winSizeHeight = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "popup_entry_general_sizey"));
    gtk_spin_button_set_value(winSizeHeight, (DOUBLE)ESSApp->options->winHeight);
    
    /* ========== Init Simulation values ========== */
	loggerAddLog(ESSApp->logger, DEBUG, "Initing Simulation Values");
    simTimeStop = GTK_ENTRY(gtk_builder_get_object(builder, "popup_entry_sim_time_stop"));
    sprintf(strSimTimeStop, "%e", ESSApp->options->simTimeStop);
    gtk_entry_set_text(simTimeStop, strSimTimeStop);

    simTimeIncrement = GTK_ENTRY(gtk_builder_get_object(builder, "popup_entry_sim_time_inc"));
    sprintf(strSimTimeIncrement, "%e", ESSApp->options->simTimeIncrement);
    gtk_entry_set_text(simTimeIncrement, strSimTimeIncrement);

    /* ========== Init Color Values  ========== */
	loggerAddLog(ESSApp->logger, DEBUG, "Initing Color Values");
    spinButtonBR = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "popup_entry_colors_bg_r"));
    spinButtonBG = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "popup_entry_colors_bg_g"));
    spinButtonBB = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "popup_entry_colors_bg_b"));
    spinButtonAR = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "popup_entry_colors_axes_r"));
    spinButtonAG = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "popup_entry_colors_axes_g"));
    spinButtonAB = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "popup_entry_colors_axes_b"));
    spinButtonLR = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "popup_entry_colors_lines_r"));
    spinButtonLG = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "popup_entry_colors_lines_g"));
    spinButtonLB = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "popup_entry_colors_lines_b"));

    gtk_spin_button_set_value(spinButtonBR, ESSApp->options->colorBg.r);
    gtk_spin_button_set_value(spinButtonBG, ESSApp->options->colorBg.g);
    gtk_spin_button_set_value(spinButtonBB, ESSApp->options->colorBg.b);
    gtk_spin_button_set_value(spinButtonAR, ESSApp->options->colorAxes.r);
    gtk_spin_button_set_value(spinButtonAG, ESSApp->options->colorAxes.g);
    gtk_spin_button_set_value(spinButtonAB, ESSApp->options->colorAxes.b);
    gtk_spin_button_set_value(spinButtonLR, ESSApp->options->colorLines.r);
    gtk_spin_button_set_value(spinButtonLG, ESSApp->options->colorLines.g);
    gtk_spin_button_set_value(spinButtonLB, ESSApp->options->colorLines.b);

    List *popupWidgetsList = newList();
    popupWidgetsList->Append(popupWidgetsList, winSizeWidth);         /* 0 */
    popupWidgetsList->Append(popupWidgetsList, winSizeHeight);        /* 1 */ 
    popupWidgetsList->Append(popupWidgetsList, simTimeStop);         /* 2 */
    popupWidgetsList->Append(popupWidgetsList, simTimeIncrement);     /* 3 */ 
    popupWidgetsList->Append(popupWidgetsList, spinButtonBR);         /* 4 */
    popupWidgetsList->Append(popupWidgetsList, spinButtonBG);         /* 5 */
    popupWidgetsList->Append(popupWidgetsList, spinButtonBB);         /* 6 */
    popupWidgetsList->Append(popupWidgetsList, spinButtonAR);         /* 7 */
    popupWidgetsList->Append(popupWidgetsList, spinButtonAG);         /* 8 */
    popupWidgetsList->Append(popupWidgetsList, spinButtonAB);         /* 9 */
    popupWidgetsList->Append(popupWidgetsList, spinButtonLR);         /* 10 */
    popupWidgetsList->Append(popupWidgetsList, spinButtonLG);         /* 11 */
    popupWidgetsList->Append(popupWidgetsList, spinButtonLB);         /* 12 */

	Popup *popupWidgets = newPopup(ESSApp, popupWidgetsList, NULL);

	loggerAddLog(ESSApp->logger, DEBUG, "Initing popup events");
    g_signal_connect(G_OBJECT(buttonClose), "button-press-event", G_CALLBACK(on_choice_click_cancel), NULL);
    g_signal_connect(G_OBJECT(buttonSaveExit), "button-press-event", G_CALLBACK(on_choice_click_save_exit), popupWidgets);
    
    gtk_widget_show_all(popup_window_options);
};
