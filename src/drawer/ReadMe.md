# Drawer

1. [Introduction](#Introduction)

2. [Draw](#Draw)

3. [DrawObject](#DrawObject)

## Introduction <a name="Introduction"></a>

Le dossier **Drawer** contient les fichiers _draw_ et _drawObject_ qui contiennent les fonctions permettant de dessinner tous les éléments de la grille comme les cercles qui représente les charges ou les lignes pour créer les axes.

## Draw <a name="Draw"></a>

Le fichier _draw_ contient les fonctions permettant de dessiner toutes les fonctionnalités dont nous avons besoin en dehors des objets.

| Type | Fonction          | Description                                              |
|------|-------------------|----------------------------------------------------------|
| VOID | draw_force_charge | Dessine une flèche en fonction de la force de la charge. |
| VOID | draw_charges      | Dessine une charge.                                      |
| VOID | draw_axes         | Dessine les axes principaux au centre de la grille.      |
| VOID | draw_lines        | Dessines les différentes lignes de la grille.            |
| VOID | draw_arrow        | Dessine une flèche.                                      |


## DrawObject <a name="DrawObject"></a>

Les fichiers _drawObject_ contient les fonctions permettant de dessiner des objets comme un cercle par exemple.

| Type | Fonction    | Description                      |
|------|-------------|----------------------------------|
| VOID | draw_circle | Dessine un cercle sur la grille. |
| VOID | draw_lines  | Dessine une ligne sur la grille. |