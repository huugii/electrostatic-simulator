/* File popupOptions.h */
/* Date: 2020-06-14 */
/* Description: popup settings */

#include "../app/app.h"
#include "../../libs/physics/src/movement/movement.h"

VOID on_popup_settings_clicked(GtkWidget *widget, App *ESSApp);
