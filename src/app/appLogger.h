/* File appLogger.h */
/* Date: 2020-06-16 */
/* Description: Logger object extra */

/* ========== Includes ========== */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "../../utils/MACRO_REF.h"
#include "../../libs/mathematics/src/list/list.h"

#define _ESS_LOG_ACTIVE_        TRUE
#define _ESS_LOG_TYPE_          INFO
#define _ESS_LOG_TEXT_SIZE_     100

#if !defined(INCLUDE_APPLOGGER_STRUCT)
#define INCLUDE_APPLOGGER_STRUCT

/* ========== Union ========== */

typedef struct AppLogger_S AppLogger;

/* ========== Variables ========== */

enum ESS_LOGGER_TYPE {
    ERROR,
    WARNING,
    INFO,
    DEBUG
};

/* ========== Structure ========== */

struct AppLogger_S {
    enum ESS_LOGGER_TYPE    type;
    BOOLEAN                 active;
    List                    *logs;
};

/* ========== Prototype of functions ========== */

AppLogger   *newLogger              ();                                                                 /* Create new logger */
VOID        loggerAddLog            (AppLogger *logger, enum ESS_LOGGER_TYPE type, STRING logText);     /* Add log in the list if allowed */

#endif /* INCLUDE_APPLOGGER_STRUCT */
