/* File draw.c */
/* Date: 2020-06-10 */
/* Description:  */

#include "draw.h"

/*
  Function: draw_force_charge
  ----------------
  Description: draw charge strength 
  Return:
 */
VOID draw_force_charge(cairo_t *cr, App *app){

	AppCharge *appChg;
	RGB color = {255, 255, 255};

	for (int idx = 0; idx < app->charges->length; idx++) {
		appChg = (AppCharge *)app->charges->GetDataByIndex(app->charges, idx);

		if(appChg->acc != NULL && appChg->acc->norm(appChg->acc)){
		    draw_arrow(cr, app, appChg, color);
		}
	}
	return;

}

/*
  Function: draw_charges
  ----------------
  Description: draw the charge
  Return:
 */
VOID draw_charges(cairo_t *cr, App *app) {

	AppCharge *appChg;
	Charge *chg;
	for (int idx = 0; idx < app->charges->length; idx++) {

		appChg = app->charges->GetDataByIndex(app->charges, idx);
		chg = appChg->chg;

		if (signbit(chg->chgq) == 0 && chg->chgq != 0) {
			draw_circle(cr, app, chg->coord, _ESS_CHARGE_RADIUS_, chg->fixe, TRUE, (appChg == app->selectedCharge), app->options->colorChargePos, app->options->colorChargeBorder);
		} else if (signbit(chg->chgq) == 1 && chg->chgq != 0) {
			draw_circle(cr, app, chg->coord, _ESS_CHARGE_RADIUS_, chg->fixe, TRUE, (appChg == app->selectedCharge), app->options->colorChargeNeg, app->options->colorChargeBorder);
		}

	}
	return;

}

/*
  Function: draw_lines
  ----------------
  Description:  draw axes at the middle of the window
  Return:
 */
VOID draw_lines(cairo_t *cr, App *app) {

	static DOUBLE dashed[] = {1.0};
	DOUBLE xOffset = app->options->posAxeX > 0 ? fmod(fabs(app->options->posAxeX), app->options->zoomGraphic) : (app->options->zoomGraphic - fmod(fabs(app->options->posAxeX), app->options->zoomGraphic));
	DOUBLE yOffset = app->options->posAxeY > 0 ? fmod(fabs(app->options->posAxeY), app->options->zoomGraphic) : (app->options->zoomGraphic - fmod(fabs(app->options->posAxeY), app->options->zoomGraphic));

	do {
		draw_line(cr, 0.1, dashed, app->options->colorLines, -1, xOffset, app->options->areaWidth, app->options->areaHeight);
		draw_line(cr, 0.1, dashed, app->options->colorLines, yOffset, -1, app->options->areaWidth, app->options->areaHeight);
		xOffset += app->options->zoomGraphic;
		yOffset += app->options->zoomGraphic;
	} while (xOffset < app->options->areaWidth || yOffset < app->options->areaHeight);

	return;
}

/*
  Function: draw_axes
  ----------------
  Description: draw axes
  Return:
 */
VOID draw_axes(cairo_t *cr, App *app) {

	draw_line(cr, 1, NULL, app->options->colorAxes, -1, app->options->posAxeX, app->options->areaWidth, app->options->areaHeight);
	draw_line(cr, 1, NULL, app->options->colorAxes, app->options->posAxeY, -1, app->options->areaWidth, app->options->areaHeight);
	return;

}

/*
  Function: draw_arrow
  ----------------
  Description:  draw arrow
  Return:
 */
VOID draw_arrow(cairo_t *cr, App *app, AppCharge *appChg, RGB color) {

	Charge *chg = appChg->chg;
	DOUBLE posX = app->options->posAxeX + chg->coord->x * app->options->zoom;
	DOUBLE posY = app->options->posAxeY - chg->coord->y * app->options->zoom;
	DOUBLE angle = atan2(appChg->acc->y, appChg->acc->x);
	DOUBLE angle2 = G_PI_2/3;
	DOUBLE posFinishX = posX + cos(angle) * 30;
	DOUBLE posFinishY = posY - sin(angle) * 30;
	angle = atan2(posFinishY-posY, posFinishX-posX);
	Point *pntStart = newPoint(posX, posY, 0);
	Point *pntFinish = newPoint(posFinishX, posFinishY, 0);
	DOUBLE dist = pntStart->distBetweenPoints(pntStart, pntFinish);
	DELETE(pntStart);
	DELETE(pntFinish);

	cairo_set_dash(cr, NULL, 0, 0);
	cairo_set_line_width(cr, 2);

	cairo_move_to(cr, (posFinishX - cos(angle - angle2) * _ESS_CHARGE_RADIUS_* _ESS_WINDOW_RATIO_), (posFinishY - sin(angle - angle2) * _ESS_CHARGE_RADIUS_ * _ESS_WINDOW_RATIO_));
    cairo_line_to(cr, posFinishX, posFinishY);
    cairo_line_to(cr, (posFinishX - cos(angle + angle2) * _ESS_CHARGE_RADIUS_ * _ESS_WINDOW_RATIO_), (posFinishY - sin(angle + angle2) * _ESS_CHARGE_RADIUS_ * _ESS_WINDOW_RATIO_));
    cairo_move_to(cr, posFinishX, posFinishY);
    cairo_line_to(cr, posX, posY);

	cairo_set_source_rgb(cr, color.r / 255, color.g / 255, color.b / 255);
	cairo_stroke(cr);
	return;

}
