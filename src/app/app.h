/* File app.h */
/* Date: 2020-06-17 */
/* Description: Application object */

/* ========== Includes ========== */

#include <gtk/gtk.h>
#include <cairo.h>
#include <math.h>

#include "appCharge.h"
#include "appOptions.h"
#include "appLogger.h"

#include "../random/random.h"
#include "../../utils/RGB.h"
#include "../../utils/MACRO_REF.h"
#include "../../libs/mathematics/src/list/list.h"

#if !defined(INCLUDE_APP_STRUCT)
#define INCLUDE_APP_STRUCT

#define _ESS_APP_VERSION_ "v1.0.0"

/* Window const */
#define _ESS_WINDOW_ZOOM_COEF_      1.2
#define _ESS_WINDOW_RATIO_          0.65

/* Charge const */
#define _ESS_CHARGE_RADIUS_         10

/* Simulation const */
#define _ESS_SIMULATION_SPEED_COEF_ 0.6

/* Glade Files */
#define _ESS_GLADE_APP_             "ui/ess_gui_app.glade"
#define _ESS_GLADE_POPUP_SETTINGS_  "ui/ess_gui_popup_settings.glade"
#define _ESS_GLADE_POPUP_CHARGES_   "ui/ess_gui_popup_charges.glade"
#define _ESS_GLADE_POPUP_EDIT_      "ui/ess_gui_popup_edit.glade"

/* ========== Unions ========== */

typedef struct App_S App;
typedef struct Popup_S Popup;

typedef enum ESS_MODES AppMode;
typedef enum ESS_SIMULATION_STATUS AppSimStatus;

/* ========== Enum Mode ========== */

enum ESS_MODES {
    DEFAULT,
    CHARGE_INSERT_FIXE,
    CHARGE_INSERT_FIXE_RAND,
    CHARGE_INSERT_MOBILE,
    CHARGE_INSERT_MOBILE_RAND,
    CHARGE_DELETE,
    CHARGE_MOVING,
    MEASUREMENT
};

enum ESS_SIMULATION_STATUS {
    STOPED,
    PAUSED,
    RUNING
};

/* ========== Struct ========== */

struct App_S {
    GtkApplication  *GtkApp;
    GtkBuilder      *Builder;

    List            *widgets;
    List            *charges;
    List            *saveCharges;

    AppCharge       *selectedCharge;
    AppOption       *options;
    AppLogger       *logger;

    BOOLEAN         isSettingChange;
    BOOLEAN         isLocked;

    AppMode         mode;
    AppSimStatus    simStatus;

    VOID            FUNC(addWidgets)    (App*, GtkWidget*);                             /* Add Widgets list in app */
    VOID            FUNC(addCharges)    (App*, Charge*);                                /* Add Charges list */
};

struct Popup_S {
    App         *parentApp;
    List        *widgets;
    List        *extraArgs;
};

App*        newApp              (GtkApplication *GApp, GtkBuilder *GBuilder);           /* Create new app 'object' */
Popup*      newPopup            (App *parentApp, List *widgets, List *extraArgs);       /* Create new popup 'object' */

VOID        displayLogs         (App *ESSApp);                                          /* Re-draw some widgets according to app->mode */
VOID        changeBtnStatus     (App *ESSApp);                                          /* Lock or Unlock buttons status */
VOID        savePopupSettings   (Popup *ESSpopup);                                      /* Save popup window options */
VOID        savePopupEdit       (Popup *ESSpopup);                                      /* Save popup window edit */
BOOLEAN     checkPopupSettings  (Popup *ESSpopup);                                      /* Check popup window options */

#endif /* INCLUDE_APP_STRUCT */