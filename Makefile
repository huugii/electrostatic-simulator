# File Makefile
# Date: 2020-05-24
# Description: makefile elecsim

# Variables Internes :
# – $@ : nom de la cible ;
# – $< : nom de la première dépendance ;
# – $ˆ : liste des dépendances ;
# – $? : liste des dépendances plus récentes que la cible ;
# – $* : nom d’un fichier sans son suffixe.

CC=gcc
AR=ar rcs
FLAGS=`pkg-config --libs --cflags gtk+-3.0` -Wall -ansi -std=c99 -Wno-deprecated-declarations -lm -Wno-unused-variable
RM=rm -f

PROJECT=elecsim
ifdef OS
	EXTENSION=exe
else
	EXTENSION=out
endif

MAIN=main.$(EXTENSION)

BIN_DIR=bin
SRC_DIR=src
OBJ_DIR=build
LIBS_DIR=libs

SRC=$(wildcard $(SRC_DIR)/*.c $(SRC_DIR)/*/*.c $(SRC_DIR)/*/*/*.c)
OBJ=$(SRC:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.o)
OBJ_LIB=$(wildcard $(LIBS_DIR)/*/$(OBJ_DIR)/*o $(LIBS_DIR)/*/$(OBJ_DIR)/*/*o $(LIBS_DIR)/*/$(OBJ_DIR)/*/*/*o )
LIB=$(BIN_DIR)/$(PROJECT).a
LIB_MATH=$(LIBS_DIR)/mathematics/bin/math.a
LIB_PHYS=$(LIBS_DIR)/physics/bin/phys.a

all:before $(MAIN) $(LIB)

# Launch main 
launch: all
	./$(MAIN)

# create main.out
$(MAIN): $(OBJ)
	$(CC) -o $@  main.c $^ $(OBJ_LIB) $(LIB_MATH) $(LIB_PHYS) $(FLAGS)

# create lib elecsim.a
$(LIB): $(OBJ)
	$(AR) $@ $< $(LIB_MATH) $(LIB_PHYS)

# create .o
$(OBJ): $(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) -o $@ -c $< $(FLAGS)

# create dir before create main.out
before: mrpropper
	mkdir -p $(BIN_DIR) $(OBJ_DIR)
	mkdir -p $(dir $(OBJ))
	@(cd $(LIBS_DIR)/mathematics && $(MAKE))
	@(cd $(LIBS_DIR)/physics && $(MAKE))

# update submodule
sync:
	git submodule sync --recursive
	git submodule update --init --remote --merge --recursive

# clean object
clean:
	@$(RM) $(OBJ)
	@$(RM) $(LIB)

mrpropper:clean
	@$(RM) $(MAIN)
	@(cd $(LIBS_DIR)/mathematics/ && $(MAKE) $@) >/dev/null || true
	@(cd $(LIBS_DIR)/physics/ && $(MAKE) $@) >/dev/null || true