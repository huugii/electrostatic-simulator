/*
 * File 		draw.h
 * Date: 		2020-06-16
 * Description: draw a basic component on the grid
 */

/* ========== Includes ========== */

#include "drawObjects.h"

/* ========== Prototype of functions ========== */

VOID    draw_force_charge   (cairo_t *cr, App *app);                                            /* Draw force arrow */
VOID    draw_charges        (cairo_t *cr, App *app);                                            /* Draw charges */

VOID    draw_axes           (cairo_t *cr, App *app);                                            /* Draw axes at the middle of the window */
VOID    draw_lines          (cairo_t *cr, App *app);                                            /* Draw lines */
VOID    draw_arrow          (cairo_t *cr, App *app, AppCharge *appChg, RGB color);              /* draw arrow on grid */
