/* File display.h */
/* Date: 2020-06-17 */
/* Description: Display functions Objects */

/* ========== Includes ========== */

#include "../app/app.h"

/* ========== Prototype of functions ========== */

VOID    set_cursor                          (GtkWidget *widget, GdkCursorType type);                                            /* Change cursor type */
VOID    set_cursor_default                  (App *ESSApp, BOOLEAN restMode, BOOLEAN unlockGrid, BOOLEAN unSelectedCharge);      /* Change cursor to default */

VOID    set_label_characterisics_charges    (App *ESSApp);                                                                      /* Display chages characteristics */
VOID    set_label_characterisics_forces     (App *ESSApp);                                                                      /* Display forces characteristics */
VOID    set_label_characterisics_electrics  (App *ESSApp, DOUBLE potential, DOUBLE intensity);                                  /* Display electrics characteristics */
List    *getAllCharges                      (App* ESSApp, INT idx);
