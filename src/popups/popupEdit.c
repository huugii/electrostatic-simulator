/* File appOptions.c */
/* Date: 2020-06-14 */
/* Description: ESSApp edit */

#include "popupEdit.h"

/*
    Function: on_choice_click_cancel
    ----------------
    Description: Cancel the edit popup (exit)
    Return: gboolean
 */
gboolean on_choose_click_cancel(GtkWidget *widget, GdkEvent *event, Popup *ESSPopup) {
    gtk_window_close(GTK_WINDOW(gtk_widget_get_toplevel(widget)));
    return GDK_EVENT_PROPAGATE;
}

/*
    Function: on_choice_click_save_exit
    ----------------
    Description: Save and Exit the popup
    Return: gboolean
 */
gboolean on_choose_click_save_exit(GtkWidget *widget, GdkEvent *event, Popup *ESSPopup) {
    loggerAddLog(ESSPopup->parentApp->logger, INFO, "Saving edit");
  	savePopupEdit(ESSPopup);
  	gtk_window_close(GTK_WINDOW(gtk_widget_get_toplevel(widget)));
    return GDK_EVENT_PROPAGATE;
}

/*
	Function: on_popup_edit_clicked
	----------------
	Description: Open 
	Return: gboolean
 */
VOID on_popup_edit_clicked(GtkWidget *widget, App *ESSApp) {

    GtkBuilder *builder;
    GtkWidget *popup_window_options;
    GtkWidget *buttonClose, *buttonSaveExit;

    GtkEntry *entryCharge, *entryMass, *entryPosX, *entryPosY, *entrySpdX, *entrySpdY;   
    GtkCheckButton *checkFixe;    

    /* ========== Init GTK ========== */
    INT status;
    builder = gtk_builder_new();
    status = gtk_builder_add_from_file(builder, _ESS_GLADE_POPUP_EDIT_, NULL);
    if (status == 0) {
      	printf("ERROR : %s not found !", _ESS_GLADE_POPUP_EDIT_);
        loggerAddLog(ESSApp->logger, DEBUG, "Exiting edit popup");
		gtk_main_quit();
    }
    
    popup_window_options = GTK_WIDGET(gtk_builder_get_object(builder, "popup_edit"));
    gtk_window_set_transient_for(GTK_WINDOW(popup_window_options), GTK_WINDOW(ESSApp->widgets->first->data));

    buttonClose = GTK_WIDGET(gtk_builder_get_object(builder, "popup_btn_close"));
    buttonSaveExit = GTK_WIDGET(gtk_builder_get_object(builder, "popup_btn_save_exit"));
    
    CHAR arr[20];

    entryCharge = GTK_ENTRY(gtk_builder_get_object(builder, "popup_entry_charge_q"));
    sprintf(arr, "%e", ESSApp->selectedCharge->chg->chgq);
    gtk_entry_set_text(entryCharge, arr);

    entryMass = GTK_ENTRY(gtk_builder_get_object(builder, "popup_entry_charge_mass"));
    sprintf(arr, "%e", ESSApp->selectedCharge->chg->mass);
    gtk_entry_set_text(entryMass, arr);

    entryPosX = GTK_ENTRY(gtk_builder_get_object(builder, "popup_entry_charge_pos_x"));
    sprintf(arr, "%e", ESSApp->selectedCharge->chg->coord->x);
    gtk_entry_set_text(entryPosX, arr);

    entryPosY = GTK_ENTRY(gtk_builder_get_object(builder, "popup_entry_charge_pos_y"));
    sprintf(arr, "%e", ESSApp->selectedCharge->chg->coord->y);
    gtk_entry_set_text(entryPosY, arr);


    entrySpdX = GTK_ENTRY(gtk_builder_get_object(builder, "popup_entry_charge_speed_x"));
    if(ESSApp->selectedCharge->initSpeed != NULL){
        sprintf(arr, "%e", ESSApp->selectedCharge->initSpeed->x);
        gtk_entry_set_text(entrySpdX, arr);
    }else{
        gtk_entry_set_text(entrySpdX, "0");
    }

    entrySpdY = GTK_ENTRY(gtk_builder_get_object(builder, "popup_entry_charge_speed_y"));
    if(ESSApp->selectedCharge->initSpeed != NULL){
        sprintf(arr, "%e", ESSApp->selectedCharge->initSpeed->y);
        gtk_entry_set_text(entrySpdY, arr);
    }else{
        gtk_entry_set_text(entrySpdY, "0");
    }

    checkFixe = GTK_CHECK_BUTTON(gtk_builder_get_object(builder, "popup_entry_charge_fixe"));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkFixe), ESSApp->selectedCharge->chg->fixe);

    List *popupWidgetsList = newList();

	Popup *popupWidgets = newPopup(ESSApp, popupWidgetsList, NULL);
    popupWidgetsList->Append(popupWidgetsList, entryCharge);
    popupWidgetsList->Append(popupWidgetsList, entryMass);
    popupWidgetsList->Append(popupWidgetsList, entryPosX);
    popupWidgetsList->Append(popupWidgetsList, entryPosY);
    popupWidgetsList->Append(popupWidgetsList, checkFixe);
    popupWidgetsList->Append(popupWidgetsList, entrySpdX);
    popupWidgetsList->Append(popupWidgetsList, entrySpdY);

	loggerAddLog(ESSApp->logger, DEBUG, "Initing popup events");
    g_signal_connect(G_OBJECT(buttonClose), "button-press-event", G_CALLBACK(on_choose_click_cancel), NULL);
    g_signal_connect(G_OBJECT(buttonSaveExit), "button-press-event", G_CALLBACK(on_choose_click_save_exit), popupWidgets);
    
    gtk_widget_show_all(popup_window_options);
};
