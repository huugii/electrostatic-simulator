/* File appextra.h */
/* Date: 2020-06-16 */
/* Description: Application object extra */

/* ========== Includes ========== */

#include <gtk/gtk.h>
#include <cairo.h>
#include <math.h>

#include "../../libs/physics/src/movement/movement.h"
#include "../../utils/RGB.h"
#include "../../utils/MACRO_REF.h"

#if !defined(INCLUDE_APPCHARGE_STRUCT)
#define INCLUDE_APPCHARGE_STRUCT

/* Charge const */
#define _ESS_CHARGE_RADIUS_     10


/* ========== Union ========== */

typedef struct AppCharge_S AppCharge;

/* ========== Structure ========== */

struct AppCharge_S {
    Charge *chg;
    
    Vector *acc;
    Vector *initSpeed;

    GtkLabel *idx;
    GtkLabel *posX;
    GtkLabel *posY;
};

/* ========== Prototype of functions ========== */

AppCharge *newAppCharge(Charge *chg, GtkLabel *idx, GtkLabel *posX, GtkLabel *posY);

#endif /* INCLUDE_APPCHARGE_STRUCT */
