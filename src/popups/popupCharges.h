/* File popupCharges.h */
/* Date: 2020-06-10 */
/* Description: popup charge */

#include "../app/app.h"
#include "../../libs/physics/src/movement/movement.h"

VOID on_popup_charges_clicked(GtkWidget *widget, App *ESSApp);
