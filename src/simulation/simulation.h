/* File simulationEvent.h */
/* Date: 2020-06-11 */
/* Description: simulation event */

/* ========== Includes ========== */

#include <stdio.h>
#include <stdlib.h>

#include "../charge/charge.h"
#include "../app/app.h"

/* ========== Prototype of functions ========== */

INT         simulation                      (void *user_data);                          /* Simulation function */                       
VOID        saveListBeforeSimulation        (App *ESSApp, List* chgsSaved);         /* Save list of charges before simulation for stop action */
