/*
 * File 		draw.h
 * Date: 		2020-06-17
 * Description: draw a basic component on the grid
 */

/* ========== Includes ========== */

#include "../app/app.h"

/* ========== Prototype of functions ========== */

VOID    draw_circle    (cairo_t *cr, App *app, Point *pt, DOUBLE radius, BOOLEAN fixe, BOOLEAN fill, BOOLEAN border, RGB color, RGB borderColor);   /* draw circle on grid */
VOID    draw_line      (cairo_t *cr, DOUBLE, const DOUBLE *, RGB, DOUBLE, DOUBLE, DOUBLE, DOUBLE);                                                  /* draw line on grid */
